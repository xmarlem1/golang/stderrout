package stage

import (
	"context"
	"os"

	"gitlab.com/ricardomendes/stderrout/logging"
)

func NewCleanupStage(ctx context.Context, logger logging.Logger) Cleanup {
	logger = logger.WithField("command", "cleanup_exec")
	logger.SetOutput(os.Stdout)

	cleanupStage := &Cleanup{
		ctx:    ctx,
		logger: logger,
	}

	return *cleanupStage
}

type Cleanup struct {
	ctx    context.Context
	logger logging.Logger
}

func (c Cleanup) Exec() error {
	c.logger.Info("The CLEANUP stage is processed here")

	return nil
}
