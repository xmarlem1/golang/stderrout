module gitlab.com/ricardomendes/stderrout

go 1.14

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
	github.com/urfave/cli v1.22.4 // indirect
	github.com/urfave/cli/v2 v2.2.0
	gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate v0.1.0
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
)
