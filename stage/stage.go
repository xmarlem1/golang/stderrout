package stage

import (
	"context"
	"fmt"
	"math/rand"
	"time"
)

const maxSimulatedCommands = 10

type Stage interface {
	Exec() error
}

func simulateScriptExecution(ctx context.Context) {
	rand.Seed(time.Now().UnixNano())
	numberOfCommands := rand.Intn(maxSimulatedCommands) + 1

	slept := make(chan bool)
	for i := 1; i <= numberOfCommands; i++ {
		fmt.Printf("Simulating command %d of %d\n", i, numberOfCommands)
		go sleep(slept)

		select {
		case <-slept:
			continue
		case <-ctx.Done():
			return
		}
	}
}

func sleep(slept chan<- bool) {
	time.Sleep(time.Second)
	slept <- true
}
