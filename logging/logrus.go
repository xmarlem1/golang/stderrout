/* This is a simplified version of
https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate/-/blob/master/internal/logging/logrus.go.

Special thanks to the GitLab Runner team for making it publicly available!*/

package logging

import (
	"io"
	"os"

	"github.com/sirupsen/logrus"
)

type Fields = logrus.Fields

func newLogrus() Logger {
	logger := logrus.New()
	logger.SetLevel(logrus.InfoLevel)
	logger.SetFormatter(newTextFormatter())

	return logrusWithLoggerAndEntry(
		logger, logger.WithField("PID", os.Getpid()))
}

type logrusLogger struct {
	*logrus.Entry

	logger *logrus.Logger
}

func logrusWithLoggerAndEntry(
	logger *logrus.Logger, entry *logrus.Entry) Logger {

	return &logrusLogger{
		Entry:  entry,
		logger: logger,
	}
}

func (l *logrusLogger) WithField(key string, value interface{}) Logger {
	return logrusWithLoggerAndEntry(l.logger, l.Entry.WithField(key, value))
}

func (l *logrusLogger) WithFields(fields Fields) Logger {
	return logrusWithLoggerAndEntry(l.logger, l.Entry.WithFields(fields))
}

func (l *logrusLogger) SetOutput(writer io.Writer) {
	l.logger.SetOutput(writer)
}

func newTextFormatter() logrus.Formatter {
	return &logrus.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	}
}
