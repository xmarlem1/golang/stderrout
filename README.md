# std err/out driver for GitLab Runner's Custom Executor

A didactic example intended to demonstrate how GitLab Runner's Custom Executor
and drivers work. This driver does nothing more than printing tracing info to
`stderr` and `stdout`.

[![pipeline
status](https://gitlab.com/ricardomendes/stderrout/badges/master/pipeline.svg)](https://gitlab.com/ricardomendes/stderrout/-/commits/master)

<!-- toc -->

- [Companion resources](#companion-resources)
- [Release channel](#release-channel)
- [Install & setup](#install--setup)
  - [Install GitLab Runner](#install-gitlab-runner)
  - [Register a Runner with the Custom Executor](#register-a-runner-with-the-custom-executor)
  - [Set up the driver](#set-up-the-driver)

<!-- tocstop -->

## Companion resources

- Blog post: [**A practical guide to GitLab Runner Custom Executor drivers** -
  _Hints and tips to create your own driver from scratch_](https://medium.com/ci-t/a-practical-guide-to-gitlab-runner-custom-executor-drivers-bc6e6562647c)

- Repository to demonstrate the driver in action:
  [gitlab.com/ricardomendes/stderrout-demo](https://gitlab.com/ricardomendes/stderrout-demo)

## Release channel

The driver is released in the form of a binary, available from an S3 bucket.
Use the commands below to download and give it permission to execute:

```sh
sudo mkdir -p /opt/gitlab-runner
sudo curl -Lo /opt/gitlab-runner/std-err-out-driver https://stderrout.s3.amazonaws.com/std-err-out-driver
sudo chmod +x /opt/gitlab-runner/std-err-out-driver
```

## Install & setup

### Install GitLab Runner

Please refer to [GitLab Runner docs](https://docs.gitlab.com/runner/install/)
for detailed assistance on how to install it.

Sample commands for **Ubuntu Linux** running on **x86-64** hardware are
presented below:

```sh
sudo mkdir -p /opt/gitlab-runner/{builds,cache}

# Download the binary
sudo curl -Lo /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permissions to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab CI user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

### Register a Runner with the Custom Executor

Once GitLab Runner is installed, it is time to [register a
Runner](https://docs.gitlab.com/runner/register/) with the [Custom
Executor](https://docs.gitlab.com/runner/executors/custom.html):

```sh
sudo gitlab-runner register

#   Enter your GitLab instance URL: https://gitlab.com or self-hosted address.

#   Enter the token to register the Runner: get it from your GitLab Project,
# Group, or Instance, by expanding the Runners section of the CI/CD settings.

#   Enter a description for the Runner. It can be changed later in GitLab’s UI.

#   Enter the tags associated with the Runner. They can be changed later in
# GitLab’s UI. Suggested tag: stderrout.

#   Enter the Runner executor: custom.
```

### Set up the driver

Use your preferred text editor to open the `/etc/gitlab-runner/config.toml`
file. Update `builds_dir`, `cache_dir`, and `[runners.custom]` as follows:

```toml
[[runners]]
  name = "__REDACTED__"
  url = "https://gitlab.com/"
  token = "__REDACTED__"
  executor = "custom"
  builds_dir = "/opt/gitlab-runner/builds"
  cache_dir = "/opt/gitlab-runner/cache"
  [runners.custom]
    config_exec = "/opt/gitlab-runner/std-err-out-driver"
    config_args = ["config"]
    prepare_exec = "/opt/gitlab-runner/std-err-out-driver"
    prepare_args = ["prepare"]
    run_exec = "/opt/gitlab-runner/std-err-out-driver"
    run_args = ["run"]
    cleanup_exec = "/opt/gitlab-runner/std-err-out-driver"
    cleanup_args = ["cleanup"]
```

[Each stage provided by the Custom
Executor](https://docs.gitlab.com/runner/executors/custom.html#stages) is
represented by a sub command in the driver's command line interface, as you can
see in the `config_args`, `prepare_args`, `run_args`, `cleanup_args` values.

Restart GitLab Runner to ensure the updated settings will be loaded:

```sh
sudo gitlab-runner restart
```
