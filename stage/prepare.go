package stage

import (
	"context"
	"fmt"

	"gitlab.com/ricardomendes/stderrout/logging"
)

func NewPrepareStage(ctx context.Context, logger logging.Logger) Prepare {
	prepareStage := &Prepare{
		ctx:    ctx,
		logger: logger.WithField("command", "prepare_exec"),
	}

	return *prepareStage
}

type Prepare struct {
	ctx    context.Context
	logger logging.Logger
}

func (p Prepare) Exec() error {
	p.logger.Info()
	fmt.Println(`
	The PREPARE stage is processed here.`)

	simulateScriptExecution(p.ctx)

	return nil
}
